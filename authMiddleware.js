const admin = require("./firebaseConfig");
const decodeToken = async(req, res, next) => {
    const rawToken = req.headers.authorization;

    const { status, err, statusCode } = await verifyToken(rawToken);
    if (!status) {
        if (statusCode == 500) {
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(statusCode).json({
                message: err
            })
        }
    }

    return next();
}


const verifyToken = async(rawToken) => {
    try {
        let decodedValue;
        const token = rawToken.split(" ")[2];
        if (rawToken.split(" ")[0] == "GAUTH") {
            decodedValue = await admin.auth().verifyIdToken(token);
        } else if (rawToken.split(" ")[0] == "NATIVEAUTH") {
            // decode with own token verification here
        }


        if (decodedValue) {
            return { err: "", status: true, statusCode: 200 };
        }
        return { err: "Unauthorized", status: false, statusCode: 401 };
    } catch (err) {
        console.log(err);
        return { err: err.message, status: false, statusCode: 500 };
    }
}

module.exports = {
    decodeToken
}