const express = require("express");
const cors = require("cors");
const { decodeToken } = require("./authMiddleware");
const app = express();
app.use(cors("*"));
app.use(express.json());

const port = 3001;

app.get("/todos", decodeToken, (req, res) => {

    return res.json({
        message: "Success",
        data: {
            todos: [{
                    title: "task 1"
                },
                {
                    title: "task 2"
                },
                {
                    title: "task 3"
                }
            ]
        }
    })
});


app.listen(port, () => {
    console.log("[INFO] Running On Port " + port)
})